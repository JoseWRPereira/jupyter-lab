# Python do Zero ao DS

## Projetos de Insights


### Etapas de um projeto de Ciência de Dados
* Questão de negócio
* Entendimento do negócio
* Coleta de dados
* Limpeza de dados
* Exploração de dados: 
  * Hipótese de negócio
  * Mapear impacto das varíáveis mais importantes, modelos, métricas, etc
* Aplicação dos modelos de ML
* Avaliação de performance do modelo de ML
* Publicação do modelo (modelo em produção).


### Projeto do tipo insights (Descobertas)

#### Objetivo
gerar insights através da análise e manipulação dos dados para auxiliar a tomada de decisão do time de negócios.

#### Etapas
* Questão de negócio
* Entendimento do negócio
* Coleta de dados
* Limpeza de dados
  * Remover dados errados/corrompidos;
  * Remover outliers devido à erros do sitema.
* Exploração de dados: 
  * Gerar hipótese para o time de negócios;
  * Mapear impacto das varíáveis mais importantes, modelos, métricas, etc;
  * Insight precisa ser acionável, caso contrário, é a penas uma curiosidade;

##### Como criar hipóteses de negócio?
* Precisa ser uma afirmação;
* Precisa ser uma comparação entre 2 variáveis;
* Você precisa definir um valor base.

Exemmplo:
H1: Imóveis que possuem vista para água, são 30% mais caros, na média.

* Afirmação: "...são ... mais caros..."
* Vars de comparação: vista para água x sem vista para água
* Valor base: 30%


## O que colocar em um projeto de portfólio?
1. Problema de negócio.
   * O que você quer resolver 

2. Premissas do negócio (escopo).
   1. O que se assume para construir a solução do problema de negócio.

3. Planejamento da solução

4. Os 5 principais insights do negócio

5. Resultados financeiros para o negócio

6. Conclusão 
   1. Seu objetivo inicial foi alcançado? Por que?

7. Próximos passos


## Recapiturando
1. Aula 01: Começando Python do zero
2. Aula 02: Extração e manipulação de dados
3. Aula 03: Transformação de dados
4. Aula 04: Estrutura de controle
5. Aula 05: Funções e organização de código
6. Aula 06: Tipos de visualização de dados
7. Aula 07: Visualização de dados
8. Aula 08: Projeto de portfólio

## Próximos passos
* Ferramentas (Linux, Jupyter Lab, Git, Github)
* Novo problema de negócio
* Desenvolvimento de pensamento analítico
* Extração de dados do html (Beautiful Soup)
* Extração de dedos de forma assíncrona
* Banco de dados, SQL com Python
* Ambiente virtual em python
* Agendamento automático de tarefas
* Primeira rodadas de pergundas de negócio do CEO e análise
* Extração de dados html via Selenium
* Segunda rodada de perguntas de negócio do CEO  e análise
* Storytelling

